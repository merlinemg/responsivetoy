from django.contrib import admin
from  app.models import NavigationMenu
from  app.models import MenuContent,Inputs,Projects,Coordinates,ProjectLines

admin.site.register(NavigationMenu)
admin.site.register(MenuContent)
admin.site.register(Inputs)
admin.site.register(Projects)
admin.site.register(Coordinates)
admin.site.register(ProjectLines)
