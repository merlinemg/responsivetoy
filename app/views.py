"""
Definition of views.
"""
import ast
import json

from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime

from django.views.generic import TemplateView

from app.forms import ProjectForm
from app.mixins import MainMixin
from  app.models import NavigationMenu, Projects, Inputs, Coordinates
from  app.models import MenuContent


# import sys
# sys.path.append('/home/pi/projects/GoAlive/PythonModules')

# import lib_TFT as TFT
# import lib_GPIO as GP
# import lib_WEBCAM as WC

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    # assert isinstance(request, HttpRequest)
    if request.POST.get("VideoAction", "") == 'setHomePage':
        TFT.OMX_QuitVideo()
        TFT.ScreenImage('default')
    elif request.POST.get("VideoPlay", "") == 'AngryBird':
        TFT.FrameBufferCopy_ON()
        TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_AngryBirds.mp4')
        TFT.OMX_SetupVideo()
        TFT.OMX_StartVideo()
    elif request.POST.get("VideoPlay", "") == 'Home':
        TFT.FrameBufferCopy_ON()
        TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_Home.mp4')
        TFT.OMX_SetupVideo()
        TFT.OMX_StartVideo()
    elif request.POST.get("VideoForward", "") == 'Home':
        TFT.FrameBufferCopy_ON()
        TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_Home.mp4')
    elif request.POST.get("VideoPause", "") != '':
        TFT.OMX_PauseVideo()
    elif request.POST.get("VideoStop", "") != '':
        TFT.OMX_QuitVideo()

    if request.POST.get("MotionPath", "") == 'STOP':
        GP.GPIO_SetupMotors()
    elif request.POST.get("MotionPath", "") == 'FRONT':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForward()
    elif request.POST.get("MotionPath", "") == 'BACK':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveBackward()
    elif request.POST.get("MotionPath", "") == 'LEFT':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForwardLeft()
    elif request.POST.get("MotionPath", "") == 'RIGHT':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForwardRight()

    if request.POST.get("CameraAction", "") == 'StartView':
        WC.StartMotionService()
    elif request.POST.get("CameraAction", "") == 'StopView':
        WC.StopMotionService()

    if request.POST.get("JqueryCallBack", "") == 'CallBack':
        print ("we got a jquery call back request")
    elif request.POST.get("JqueryCallBack", "") == 'StopView':
        WC.StopMotionService()

    return render(
        request,
        'app/index.html',
        {
            'title': 'Home Page',
            'year': datetime.now().year,
        }
    )


def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title': 'Contact',
            'message': 'Your contact page.',
            'year': datetime.now().year,
        }
    )


def playvideo(request):
    """Renders the Play Videos page."""
    assert isinstance(request, HttpRequest)
    if request.POST.get("VideoAction", "") == 'setHomePage':
        TFT.OMX_QuitVideo()
        TFT.ScreenImage('default')
    elif request.POST.get("VideoAction", "") == 'AngryBird':
        TFT.FrameBufferCopy_ON()
        TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_AngryBirds.mp4')
        TFT.OMX_SetupVideo()
        TFT.OMX_StartVideo()
    elif request.POST.get("VideoAction", "") == 'Home':
        TFT.FrameBufferCopy_ON()
        TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_Home.mp4')
        TFT.OMX_SetupVideo()
        TFT.OMX_StartVideo()

    if request.POST.get("MotorAction", "") == 'SetupMotors':
        GP.GPIO_SetupMotors()
    elif request.POST.get("MotorAction", "") == 'MoveFront':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForward()
    elif request.POST.get("MotorAction", "") == 'MoveBack':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveBackward()
    elif request.POST.get("MotorAction", "") == 'TurnLeft':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForwardLeft()
    elif request.POST.get("MotorAction", "") == 'TurnRight':
        GP.GPIO_SetupMotors()
        GP.GPIO_MoveForwardRight()

    if request.POST.get("CameraAction", "") == 'StartView':
        WC.StartMotionService()
    elif request.POST.get("CameraAction", "") == 'StopView':
        WC.StopMotionService()

    if request.POST.get("JqueryCallBack", "") == 'CallBack':
        print ("we got a jquery call back request")
    elif request.POST.get("JqueryCallBack", "") == 'StopView':
        WC.StopMotionService()

    category_list = NavigationMenu.objects.all().order_by('-NavOrder')
    content_list = MenuContent.objects.all().select_related('Name')

    NavMenu_dict = {'NavigationMenu': category_list}
    return render(
        request,
        'app/playvideo.html',
        {
            'title': 'Responsive Toys',
            'message': 'Select Movie you want to watch',
            'year': datetime.now().year,
            'NavMenu': category_list,
            'MenuContent': content_list,
        }
    )


class AboutView(TemplateView, MainMixin):
    template_name = 'app/about.html'

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        context['title'] = 'About'
        context['message'] = 'Your application description page.'
        context['year'] = datetime.now().year
        context['inputs'] = Inputs.objects.all().order_by('priority')
        context['projects'] = Projects.objects.all()
        context['coordinate_dic'] = self.get_project_coordinates_and_lines_in_json()
        return context


def DrawPathIframe(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/DrawPathIframe.html',
        {
            'title': 'About',
            'message': 'Your application description page.',
            'year': datetime.now().year,

        }
    )


class SaveProject(TemplateView, MainMixin):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('method') == 'save':
                project = self.is_project_exist_or_create(request.POST.get('projectID'))
                project.project_name = request.POST.get('projectName')
                project.save()
                message = {'status': True, 'projectName': project.project_name, 'projectID': project.pk}
                return HttpResponse(json.dumps(message),
                                    content_type='application/json',
                                    )
            elif request.POST.get('method') == 'delete':
                self.delete_items(Projects, request.POST.get('projectID'))
                message = {'status': True}
                return HttpResponse(json.dumps(message), content_type='application/json', )

            elif request.POST.get('method') == 'coordinates':
                if request.POST.get('inputs'):
                    inputs = ast.literal_eval(request.POST.get('inputs'))
                    for key in inputs:
                        for data in inputs[key]:
                            coordinates = self.is_coordinate_exist_or_create(data.get('uid'), data.get('project'))

                            coordinates.x_cordinate = data.get("points").get('x')
                            coordinates.y_cordinate = data.get("points").get('y')
                            coordinates.new_id = data.get('uid')
                            coordinates.project = self.is_project_exist_or_create(data.get('project'))
                            coordinates.input_id = self.get_inputObject_by_id(data.get('inputid'))
                            coordinates.save()
                if request.POST.get('lines'):
                    lines = ast.literal_eval(request.POST.get('lines'))
                    for key in lines:
                        for data in lines[key]:

                            project_line = self.is_projectLines_exist_or_create(data.get('uid'), data.get('project'))
                            project_line.x_coordinate_start = data.get('startPoints').get('x')
                            project_line.x_coordinate_end = data.get('endPoints').get('x')
                            project_line.y_coordinate_start = data.get('startPoints').get('y')
                            project_line.y_coordinate_end = data.get('endPoints').get('y')
                            project_line.start_input = self.get_coordinate_by_newId(data.get('connectWith').get('start'))
                            project_line.end_input = self.get_coordinate_by_newId(data.get('connectWith').get('end'))
                            project_line.new_id = data.get('uid')
                            project_line.project = self.is_project_exist_or_create(data.get('project'))
                            project_line.save()

                response_json = {}
                response_json['inputs'] = request.POST.get('inputs')
                response_json['lines'] = request.POST.get('lines')
                message = {'status': True, 'response_json': response_json}

                return HttpResponse(json.dumps(message), content_type='application/json', )

    def update_modified_time(self, model, user):
        model.modified_timestamp = datetime.now()
        model.modified_by = user

    def update_created_time(self, model, user):
        model.creation_timestamp = datetime.now()
        model.created_by = user


class UpdateCockpit(TemplateView):
    def post(self, request, *args, **kwargs):
        message = {'status': True}
        return HttpResponse(json.dumps(message), content_type='application/json', )
