import json

from app.models import Projects, Inputs, Coordinates, ProjectLines


class MainMixin(object):
    def get_all_projects(self):
        return Projects.objects.all()

    def get_inputObject_by_id(self, input_id):

        input = Inputs.objects.get(pk=input_id)

        return input

    def is_project_exist_or_create(self, terms):
        if terms != '0':
            project = Projects.objects.get(pk=terms)
        else:
            project = Projects()

        return project

    def get_project_by_id(self, project_id):

        try:
            project = Projects.objects.get(pk=project_id)
        except:
            project = None
        return project

    def delete_items(self, models, terms):

        project_to_delete = models.objects.get(pk=terms)
        project_to_delete.delete()

        return True

    def get_input_by_newid_and_project(self, input_id, project):

        try:
            inputs = Inputs.objects.get(pk=input_id)
        except:
            inputs = None
        return inputs

    def is_coordinate_exist_or_create(self, newid, project):
        projects = self.get_project_by_id(project)
        try:
            coordinate = Coordinates.objects.get(new_id=newid, project=projects)
        except:
            coordinate = Coordinates()

        return coordinate

    def is_projectLines_exist_or_create(self, newid, project):
        projects = self.get_project_by_id(project)
        try:
            return ProjectLines.objects.get(new_id=newid, project=projects)
        except:
            return ProjectLines()

    def get_project_lines(self):
        """"""
        """

        :return: Project Line Dictionary
        """
        project_dict = {}
        flag = 0
        tt = {"19": {"1486377000591": [
            {"uid": 1486377000591, "startPoints": {"x": 272, "y": 142}, "endPoints": {"x": 502, "y": 155},
             "project": 19, }]}}
        projects = self.get_all_projects()
        for project in projects:
            pickup_dicts = {}
            if project.project_lines.all():
                for project_line in project.project_lines.all():
                    pickup_dicts.update({str(project_line.new_id): [{"uid": project_line.new_id, "startPoints": {
                        "x": project_line.x_coordinate_start, "y": project_line.y_coordinate_start},
                                                                     "endPoints": {"x": project_line.x_coordinate_end,
                                                                                   "y": project_line.y_coordinate_end},
                                                                     "connectWith": {
                                                                         "start": self.get_coordinate_by_projectLine(
                                                                             project_line),
                                                                         "end": self.get_coordinate_by_projectLine(
                                                                             project_line)},

                                                                     "project": project_line.project.pk}]})

                project_dict.update({str(project.id): pickup_dicts})
                flag = 1
        project_dict = str(json.dumps(project_dict))
        if flag == 0:
            project_dict = {}
        return project_dict

    def get_project_coordinates_and_lines_in_json(self):
        """"""

        """

        :param project:
        :return:
        """

        project_dict = {}
        flag = 0
        response_dict = {}
        project = self.get_all_projects()
        for prjcts in project:

            pickup_dict = {}
            pickup_dicts = {}
            if prjcts.project_coordinates.all():
                for coordinates in prjcts.project_coordinates.all():
                    pickup_dicts.update({str(coordinates.new_id): [{"uid": coordinates.new_id,
                                                                    "points": {"x": coordinates.x_cordinate,
                                                                               "y": coordinates.y_cordinate},
                                                                    "inputid": coordinates.input_id.pk,
                                                                    "project": coordinates.project.pk}]})

                pickup_dict[str(coordinates.new_id)] = pickup_dicts
                project_dict.update({str(prjcts.id): pickup_dicts})
                flag = 1
        project_dict = str(json.dumps(project_dict))
        if flag == 0:
            project_dict = {}
        response_dict['inputs'] = project_dict
        response_dict['lines'] = self.get_project_lines()
        return response_dict

    def get_coordinate_by_newId(self, new_id):

        try:
            return Coordinates.objects.get(new_id=new_id)
        except:
            return None

    def get_coordinate_by_projectLine(self, project):
        try:
            return project.start_input.new_id
        except:
            return None
