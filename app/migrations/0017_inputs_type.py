# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-04 11:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20170201_0519'),
    ]

    operations = [
        migrations.AddField(
            model_name='inputs',
            name='type',
            field=models.CharField(choices=[(b'Inputs', b'Inputs'), (b'Actions', b'Actions'), (b'Utility', b'Utility')], default=1, max_length=150, verbose_name='type'),
            preserve_default=False,
        ),
    ]
