# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-06 04:31
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20170205_0740'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='projectlines',
            options={'verbose_name': 'ProjectLine', 'verbose_name_plural': 'ProjectLines'},
        ),
    ]
