from time import sleep
import RPi.GPIO as GPIO
import smbus
import os

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False) 

#Setting the communication to BCM mode,
#User need not worry about the actual IC pin
#####################################################################################

#Switches Connection
#When user Press an Input button the voltage on the GPIO pin goes from 3.1v to GND, triggerring an event
# SWx = xx (GPIOxx)
lstSW = {}
#lstSW['SW1'] = 17
#lstSW['SW2'] = 27
#lstSW['SW3'] = 22
#lstSW['SW4'] = 23
#lstSW['SW5'] = 12
#lstSW['SW6'] = 13
#lstSW['SW7'] = 21

lstSW['SW1'] = 12
lstSW['SW2'] = 17
lstSW['SW3'] = 13
lstSW['SW4'] = 27
lstSW['SW5'] = 23
lstSW['SW6'] = 22
lstSW['SW7'] = 21

######################################################################################

#Motor Connection
#All Motors are powered through an external power supply
#The Motion is controlled in a H-Bridge 
#GPIOs are configured in PWN mode to generate the bit patterns for running motors.
lstMO = {}
lstMO['M1_EN']  = 5
lstMO['M2_EN']  = 6
lstMO['M1_FWD'] = 19
lstMO['M2_FWD'] = 16
lstMO['M1_REV'] = 26
lstMO['M2_REV'] = 20
#######################################################################################

#MCP I2C address
#H/W configuration for the MCP allows 2 banks A & B to be used as ip/op channels
#MCP1 @ 0x20, only has Bank A dedicated to the PIR sensors (Constraint)
#
bus = smbus.SMBus(1)

lstMCP = {}
lstMCP['MCP1'] = '0x20'
lstMCP['MCP2'] = '0x21'
lstMCP['MCP3'] = '0x22'
lstMCP['MCP4'] = '0x23'

MCP_IODIRA = '0x00' # This controls if a GPIO is in input(1) or output(0) mode
MCP_OLATA = '0x14'
MCP_GPIOA = '0x12'

MCP_IODIRB = '0x01'
MCP_OLATB = '0x15'
MCP_GPIOB = '0x13'
#######################################################################################
# MISC Environment variables

_inputBlocking = "False"

#######################################################################################
#Support Functions

def os_runcmd(cmdstring):
  p = os.popen(cmdstring,"r")
  return True

#######################################################################################
#MCP Controlller Functions
MCP_OLAT = {}
MCP_OLAT['A'] = MCP_OLATA
MCP_OLAT['B'] = MCP_OLATB

def MCP_Setup():
  for k,v in lstMCP.items():
    if k=='MCP1':
      os_runcmd('sudo i2cset -y 1 '+v+' '+MCP_IODIRA+' 0x01')
      os_runcmd('sudo i2cset -y 1 '+v+' '+MCP_IODIRB+' 0x00')
    else:
      os_runcmd('sudo i2cset -y 1 '+v+' '+MCP_IODIRA+' 0x00')
      os_runcmd('sudo i2cset -y 1 '+v+' '+MCP_IODIRB+' 0x00')
def MCP_WriteOut(mcpID,mcpBank,mcpValue):
  if mcpBank == 'A':
    bank = MCP_OLATA
  if mcpBank == 'B':
    bank = MCP_OLATB  
  os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+bank+' '+mcpValue)

def MCP_TurnOff(mcpID,mcpBank,mcpValue):
  if mcpBank == 'A':
    bank = MCP_OLATA
  if mcpBank == 'B':
    bank = MCP_OLATB
  os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+bank+' 0x00')

def MCP_TurnOffAll():
  for k,v in lstMCP.items():
    MCP_TurnOff(k,'A','0x00')
    MCP_TurnOff(k,'B','0x00')

def MCP_LightMode(mcpID,mcpbank,mode):
  if mode == 'countup':
    for indx in range(0,63):
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' '+str(hex(indx)))
      sleep(0.2)
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' 0x00')
      sleep(0.1)
  if mode == 'sweep':
    dissum = 0
    for indx in range(0,6):
      dissum = dissum + 2**indx
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' '+str(hex(dissum)))
      sleep(0.2)
    for indx in range(0,6):
      dissum = dissum - 2**indx
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' '+str(hex(dissum)))
      sleep(0.2)
  if mode == 'blink':
    for indx in range(0,5):
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' 0xFF')
      sleep(0.1)
      os_runcmd('sudo i2cset -y 1 '+lstMCP[mcpID]+' '+MCP_OLAT[mcpbank]+' 0x00')
      sleep(0.1)


#######################################################################################

#Below are function which control the Motor Motion. 

def GPIO_SetupMotors():
  for k,v in lstMO.items():
    GPIO.setup(v, GPIO.OUT, initial=GPIO.LOW)
    GPIO.output(v,False)
  GPIO.output(lstMO['M1_EN'],True)
  GPIO.output(lstMO['M2_EN'],True)

def GPIO_MoveForward():
  GPIO_PowerMotor_ON('M1_FWD')
  GPIO_PowerMotor_ON('M2_FWD')

def GPIO_MoveBackward():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_MoveForwardLeft():
  GPIO_PowerMotor_ON('M1_FWD')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_MoveForwardRight():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_MoveBackward():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')


def GPIO_MoveBackward():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_MoveBackward():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_MoveBackward():
  GPIO_PowerMotor_ON('M1_REV')
  GPIO_PowerMotor_ON('M2_REV')

def GPIO_RunMotors():
  GPIO_PowerMotor_ON('M1_FWD')
  sleep(5)
  GPIO_PowerMotor_OFF('M1_FWD')
  #Second Motor
  GPIO_PowerMotor_ON('M2_FWD')
  sleep(5)
  GPIO_PowerMotor_OFF('M2_FWD')

def GPIO_PowerMotor_ON(Motor_Direction):
  print "Start Block in GPIO"
  global _inputBlocking
#  _inputBlocking = "True"
  GPIO.output(lstMO[Motor_Direction],True)
  sleep(1)
#  print "Block Stop in GPIO"
#  _inputBlocking = "False"

def GPIO_PowerMotor_OFF(Motor_Direction):
  global _inputBlocking
#  _inputBlocking = "True"  
  GPIO.output(lstMO[Motor_Direction],False)
  sleep(1)
#  _inputBlocking = "False"


def GPIO_DisableMotors():
  _inputBlocking = "True"
  GPIO.output(lstMO['M1_REV'],False)
  GPIO.output(lstMO['M2_REV'],False)
  GPIO.output(lstMO['M2_FWD'],False)
  GPIO.output(lstMO['M2_EN'],False)
  GPIO.output(lstMO['M1_FWD'],False)
  GPIO.output(lstMO['M1_EN'],False)
  _inputBlocking = "False"

#Define all the Input switches to Pull Up mode,as their default state is High at 3V
#Define Event Call Back Functions for each of the switches.
#These Functions will be defined and overridden in the implementing Controller function
#These user input buttons will have different actions based on the mode of the device and current operation

def GPIO_SetupSW():
  for k,v in lstSW.items():
    #print 'setting up switch: ' + k + '@' + str(v)
    GPIO.setup(v,GPIO.IN,pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(v,GPIO.FALLING,bouncetime=500)
  #Below functions are defined local in the GPIO module where global actions can be controlled
  #GPIO.add_event_callback(lstSW["SW1"], fn_SW1_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW2"], fn_SW2_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW3"], fn_SW3_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW4"], fn_SW4_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW5"], fn_SW5_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW6"], fn_SW6_INP_RCVED)
  #GPIO.add_event_callback(lstSW["SW7"], fn_SW7_INP_RCVED)

def GPIO_RemoveSW():
  for k,v in lstSW.items():
    GPIO.remove_event_detect(v)

def GPIO_CheckSWClick(SWID):
  if(GPIO.event_detected(lstSW[SWID])):
    return 1
  else:
    return 0

def GPIO_Cleanup():
  GPIO.cleanup()


