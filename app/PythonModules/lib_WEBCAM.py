import os as os
import time as time
import subprocess as sub
import thread

MotionWebCamStatus = "NotRunning"

def os_runcmd(cmdstring):
  p = os.popen(cmdstring)
  return True

def StartMotionService():
  global MotionWebCamStatus
  try:
    if MotionWebCamStatus == "NotRunning":
      cmd2run = "sudo service motion start"
      os_runcmd(cmd2run)
      MotionWebCamStatus = "Running"
      return "success"
  except Exception,e: 
    print str(e)
    #Write to Log file"
    return "failure"
   
def StopMotionService():
  global MotionWebCamStatus
  try:
    if MotionWebCamStatus == "Running":
      cmd2run = "sudo service motion stop"
      os_runcmd(cmd2run)
      MotionWebCamStatus = "NotRunning"
      return "success"
  except Exception,e: 
    print str(e)
    #Write to Log file"
    return "failure"
