import os as os
import time as time
import subprocess as sub
import thread

FrameBufferCopyStatus = "OFF"
VideoFileToPlay = ""
omxfifo = '/home/pi/Downloads/videos/omxfifo'

def os_runcmd(cmdstring):
  p = os.popen(cmdstring,"r")
  return True

def FrameBufferCopy_ON():
  global FrameBufferCopyStatus
  if FrameBufferCopyStatus == "OFF":
    if(os_runcmd("fbcp &")):
      FrameBufferCopyStatus = "ON"
    else:
      os_runcmd('killall fbcp')
      time.sleep(1)
      os_runcmd('fbcp &')

def FrameBufferCopy_OFF():
  global FrameBufferCopyStatus
  if FrameBufferCopyStatus == "ON":
    if(os_runcmd('killall fbcp')):
      FrameBufferCopyStatus = "OFF"

def OMX_SetVideoFile(filepath):
  global VideoFileToPlay
  if(os.path.exists(filepath)):
    VideoFileToPlay = filepath
  else:
    print "Video file not found in path provided"

def OMX_SetupVideo_fifo(fifopath):
  global VideoFileToPlay
  os_runcmd('omxplayer --vol -3000 '+ VideoFileToPlay + ' < ' + fifopath)

def OMX_SetupVideo():
  global VideoFileToPlay
  try:
    if os.path.exists (omxfifo): os.system("rm " + omxfifo )
    os.system("mkfifo " + omxfifo )
    thread.start_new_thread(OMX_SetupVideo_fifo,(omxfifo,))
  except:
    print "Unable to launch OMX player on independent thread"

def OMX_CommEcho(cmd2send):
  os_runcmd('echo -n ' + cmd2send + ' > ' + omxfifo)

def OMX_StartVideo():
  OMX_CommEcho('p')
  OMX_CommEcho('p')

def OMX_PauseVideo():
  OMX_CommEcho('p')

def OMX_PlayVideo():
  OMX_CommEcho('p')

def OMX_QuitVideo():
  OMX_CommEcho('q')
  time.sleep(1)
  FrameBufferCopy_OFF()
  time.sleep(1)
  ScreenImage('default')


def ScreenImage(tag):
  imgcmd = 'sudo fbi -T 2 -d /dev/fb1 --noverbose -a '
  if tag=='default':
    os_runcmd(imgcmd + '/home/pi/Downloads/images/robo.jpg')
  if tag=='happy':
    os_runcmd(imgcmd + '/home/pi/Downloads/images/smile.jpg')
  if tag=='scary':
    os_runcmd(imgcmd + '/home/pi/Downloads/images/Scary1.jpg')

