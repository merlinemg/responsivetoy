"""
Definition of models.
"""
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class NavigationMenu(models.Model):
    NavMenu = models.CharField(max_length=64, unique=True)
    NavOrder = models.IntegerField(default=1)
    NavTitle = models.CharField(max_length=128)
    NavIcon = models.CharField(max_length=128)
    NavColor = models.CharField(max_length=128)

    def __unicode__(self):
        return self.NavMenu


class MenuContent(models.Model):
    Name = models.ForeignKey(NavigationMenu)
    Title = models.CharField(max_length=128)
    Content = models.CharField(max_length=8128)

    def __unicode__(self):
        return self.Title


class BaseTimeStamp(models.Model):
    """
    base model for timestamp and creation...
    """
    creation_timestamp = models.DateTimeField(_('Creation timestamp'), null=True, blank=True)
    modified_timestamp = models.DateTimeField(_('Modified timestamp'), null=True, blank=True)
    created_by = models.ForeignKey(User, null=True, blank=True, related_name='created_user')
    modified_by = models.ForeignKey(User, null=True, blank=True, related_name='modified_user')


class Inputs(BaseTimeStamp):
    TYPES = (
        ('Inputs', 'Inputs'),
        ('Actions', 'Actions'),
        ('Utility', 'Utility'),
    )

    name = models.CharField(_('Name'), max_length=200)
    icon = models.ImageField(_('input icons'), upload_to="icons")
    priority = models.IntegerField(_('Priority'))
    identifier = models.CharField(_('identifier'), max_length=200)
    type = models.CharField(_('type'), max_length=150, choices=TYPES)

    class Meta:
        verbose_name = 'Input'
        verbose_name_plural = 'Inputs'

    def __unicode__(self):
        return self.name


class Projects(BaseTimeStamp):
    project_name = models.CharField(_('Project Name'), max_length=300)

    class Meta:
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

    def __unicode__(self):
        return self.project_name


class Coordinates(BaseTimeStamp):
    x_cordinate = models.FloatField(_('X-Coordinate'))
    y_cordinate = models.FloatField(_('Y-Coordinate'))
    project = models.ForeignKey(Projects, related_name='project_coordinates')
    input_id = models.ForeignKey(Inputs, related_name='input_coordinate')
    new_id = models.PositiveIntegerField(_('New Id'))

    class Meta:
        verbose_name = 'Coordinate'
        verbose_name_plural = 'Coordinates'

    def __unicode__(self):
        return "{0} -> {1}".format(self.project.project_name, self.new_id)


class ProjectLines(BaseTimeStamp):
    project = models.ForeignKey(Projects, related_name='project_lines')
    x_coordinate_start = models.FloatField(_('X-Coordinate Start'))
    x_coordinate_end = models.FloatField(_('X-Coordinate End'))
    y_coordinate_start = models.FloatField(_('Y-Coordinate Start'))
    y_coordinate_end = models.FloatField(_('Y-Coordinate End'))
    new_id = models.PositiveIntegerField(_('New Id'))
    start_input = models.ForeignKey(Coordinates,related_name='start_input')
    end_input = models.ForeignKey(Coordinates,related_name='end_input')

    class Meta:
        verbose_name = 'ProjectLine'
        verbose_name_plural = 'ProjectLines'

    def __unicode__(self):
        return self.project.project_name
