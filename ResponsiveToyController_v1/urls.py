"""
Definition of urls for ResponsiveToyController_v1.
"""

from datetime import datetime

from django.conf import settings
from django.conf.urls import url
import django.contrib.auth.views
from django.conf.urls.static import static

import app.forms
import app.views

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin

from app import views

admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^playvideo$', app.views.playvideo, name='playvideo'),
    url(r'^DrawPathIframe', app.views.DrawPathIframe, name='DrawPathIframe'),
    url(r'^about', app.views.AboutView.as_view(), name='about'),
    url(r'^login/$',
        django.contrib.auth.views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
        django.contrib.auth.views.logout,
        {
            'next_page': '/',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^project-save', app.views.ProjectSave, name='projectsave'),
    url(r'^project-save', views.SaveProject.as_view(), name='saveproject'),
    url(r'^cockpit-update', views.UpdateCockpit.as_view(), name='updatecockpit'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
