import time
import lib_GPIO as MCP

try:
  MCP.MCP_Setup()
  for indx2 in range (0,2):
    MCP.MCP_LightMode('MCP1','B','sweep')
    MCP.MCP_LightMode('MCP1','B','blink')
    MCP.MCP_LightMode('MCP1','B','countup')
except:
  print "Exe except stmt block"
  MCP.MCP_TurnOffAll()
finally:
  print "Exe Finally stmt block"
  MCP.MCP_TurnOffAll()
