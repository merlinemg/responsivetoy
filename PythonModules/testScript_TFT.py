
import time
import lib_TFT as TFT
import lib_GPIO as SW
import RPi.GPIO as GPIO
from datetime import datetime


def Setup_GPIO_InputFn():
  GPIO.add_event_callback(SW.lstSW["SW1"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW2"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW3"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW4"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW5"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW6"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW7"], fnCNTRL_INP_RCVED)

print "Setting up Screen Buffer"
TFT.ScreenImage('scary')
time.sleep(2)
TFT.ScreenImage('happy')

#TFT.FrameBufferCopy_ON()
#time.sleep(1)

#print "Setting up the video file"
#TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_IndependenceDay.mp4')
#time.sleep(1)

#print "Setting up omxplayer with fifo"
#print(TFT.OMX_SetupVideo())
#time.sleep(1)

#print "Starting thevideo"
#TFT.OMX_StartVideo()
ExitControl ='False'

def fnCNTRL_INP_RCVED(channel):
  inpSW = ''
  global ExitControl

  for k,v in SW.lstSW.items():
    if v==channel:
      inpSW =  k
      print "INP => "+k 
  if inpSW == 'SW5':
    ExitControl = 'True'
  if inpSW == 'SW2':
    TFT.FrameBufferCopy_ON()
    time.sleep(0.1)
    print "Setting up the video file"
    TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_IndependenceDay.mp4')
    time.sleep(0.1)
    print "Setting up omxplayer with fifo"
    print(TFT.OMX_SetupVideo())
    time.sleep(0.1)
    print "Starting thevideo"
    TFT.OMX_StartVideo()
  if inpSW == 'SW3':
    SW.GPIO_SetupMotors()
    time.sleep(1)
    SW.GPIO_RunMotors()
    time.sleep(3)
    SW.GPIO_DisableMotors()

print "Setting up the SW GPIOS"
SW.GPIO_SetupSW()
Setup_GPIO_InputFn()


while 1:
  if ExitControl == 'True':
    break;
