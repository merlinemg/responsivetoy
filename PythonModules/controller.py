#########################################################################################################################################
# Module Functions
# 1. Auto Launched on Start up and allows for Conigurations like: WiFi, 
# 2. GUI controller: Responsible for displaying the Screen Menu and also takes input from the buttons
# 3. Starts or Stops the Webserver. which is the portal for control via Phone and also for adding more content
# 4. 
# 5. 
# 6. 
# 7. 
# 8. 
##########################################################################################################################################
# Versions Details
# 1. 
# 2. 
# 3. 
##########################################################################################################################################

import time
import lib_TFT as TFT
import lib_GPIO as SW
import RPi.GPIO as GPIO
from datetime import datetime

def Setup_GPIO_InputFn():
  GPIO.add_event_callback(SW.lstSW["SW1"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW2"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW3"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW4"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW5"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW6"], fnCNTRL_INP_RCVED)
  GPIO.add_event_callback(SW.lstSW["SW7"], fnCNTRL_INP_RCVED)


GUI_MODE = "VIDEO_PLAYBACK"
GUI_MENU_LEVEL = "1"
QuitMovie = 'False'

def fnCNTRL_INP_RCVED(channel):
  if SW._inputBlocking == "True":
    print "current in blocking " + str(channel) + "             "+datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return

  global GUI_MODE,GUI_MENU_LEVEL,QuitMovie
  inpSW = ''
  for k,v in SW.lstSW.items():
    if v==channel:
      inpSW =  k
      print "INP => "+k + " Curr In: " + GUI_MODE + " Blocking: " + SW._inputBlocking
  if GUI_MODE == "VIDEO_PLAYBACK" and GUI_MENU_LEVEL == "1":
    if inpSW == "SW1":
      TFT.OMX_PauseVideo()
    if inpSW == "SW2":
      QuitMovie = 'True'
    if inpSW == "SW5":
      GUI_MODE = "MOTION_CONTROL"
      GUI_MENU_LEVEL = "1"
      print "Changed the Menu Modes to MOTION CONTROL"
  elif GUI_MODE == "MOTION_CONTROL" and GUI_MENU_LEVEL == "1":
    if inpSW == "SW6":
      SW.GPIO_RunMotors()
    if inpSW == "SW2":
      QuitMovie = 'True'
    if inpSW == "SW5":
      GUI_MODE = "VIDEO_PLAYBACK"
      GUI_MENU_LEVEL = "1"
      print "Changed Menu Mode to VIDEO PLAYBACK"

chk_TFT = 'True'
chk_GPIO = 'True'
chk_MOTOR = 'True'

if chk_TFT == 'True':
  print "Setting up Screen Buffer"
  TFT.ScreenImage('default')
  time.sleep(1)

  TFT.FrameBufferCopy_ON()
  time.sleep(1)

  print "Setting up the video file"
#  TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_Home.mp4')
  TFT.OMX_SetVideoFile('/home/pi/Downloads/videos/Trailer_IndependenceDay.mp4')
  time.sleep(1)

  print "Setting up omxplayer with fifo"
  print(TFT.OMX_SetupVideo())
  time.sleep(1)

  print "Setting up the SW GPIOS"
  SW.GPIO_SetupSW()
  Setup_GPIO_InputFn()

  print "Setting up the Motors Enabled"
  SW.GPIO_SetupMotors()

  print "Starting the video"
  TFT.OMX_StartVideo()
  
  # This loop keep the program in an endless loop and the events are being monitored wihtou much CPU pooling.
  while 1:
    if QuitMovie == 'True':
      print "Quiting Movie"
      TFT.OMX_QuitVideo()
      SW.GPIO_DisableMotors()
      break

SW.GPIO_Cleanup()
    
print "Stopping the Screen buffer copy"
TFT.FrameBufferCopy_OFF()
time.sleep(1)

TFT.ScreenImage('default')

  
