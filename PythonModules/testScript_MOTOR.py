
import time
import lib_TFT as TFT
import lib_GPIO as SW
import RPi.GPIO as GPIO
from datetime import datetime


SW.GPIO_SetupMotors()
time.sleep(1)
SW.GPIO_RunMotors()
time.sleep(3)
SW.GPIO_DisableMotors()
time.sleep(2)
SW.GPIO_Cleanup()
print "Setting up the SW GPIOS"
